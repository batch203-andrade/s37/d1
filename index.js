// Require installed packages
require('dotenv').config();
const express = require('express');
const app = express();
const cors = require('cors');
const mongoose = require('mongoose');

// Setup MiddleWare
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

// Setup MongoDB Connection thru Mongoose
const DB_URL = process.env.DB_URL;
mongoose.connect(DB_URL, { useNewUrlParser: true, useUnifiedTopology: true });

// Initialize connection to database
const db = mongoose.connection;
db.on("error", console.error.bind(console, "There was an error connecting to the database."));
db.once("open", () => console.log("Successfully Connected to the Database"));


let User = mongoose.model("user", UserSchema);

const router = express.Router();


app.post('/createUser', (req, res) => {

    let newUser = new User({
        username: req.body.username,
        password: req.body.password,
    });

    newUser.save((saveErr, savedUser) => {
        if (saveErr) res.send(saveErr.message);
        else res.send(savedUser);
    });

})
// Assign Port
const port = process.env.PORT || 3001;

// Listen to Port
app.listen(port, () => console.log("Listening to port:" + port));